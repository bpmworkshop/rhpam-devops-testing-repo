DevOps Testing project.
=======================

The online workshop code repository for the devops testing project ((https://bpmworkshop.gitlab.io/rhpam-devops-workshop).

[![Cover Slide](https://gitlab.com/bpmworkshop/rhpam-devops-workshop/raw/master/cover.png)](https://bpmworkshop.gitlab.io/rhpam-devops-workshop)
